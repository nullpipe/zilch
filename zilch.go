// Package zilch does nothing.
package zilch // import "go.nullpipe.dev/zilch"

import "log"

// Nada prints "Nada does nothing." to the log.
func Nada() {
	log.Println("Nada does nothing.")
}
